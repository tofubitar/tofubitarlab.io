---
title: 46elks blandat
date: 2021-01-09 18:00:00
tags: 
---

## Många termer och grejs

När jag har kollat på det här så har jag råkat på många termer:

* VoIP
* SIP
* SIP-trunk
* Elastic SIP-trungk
* SIP-trunking provider
* SIP proxy
* PBX
* PSTN
* ENUM+SIP

Om/när jag får rätt på det här så hoppas jag kunna beskriva bättre vad alla termer betyder och vilka som är av betydelse.