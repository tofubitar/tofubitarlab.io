---
title: Info
date: 2021-01-02 15:03:13
---
Den här sidan skapade jag för att dela med mig lite av bitar av livet. Fokus på programmering, integritet och fri mjukvara. Jag nås på [@samuel@mastodon.se](https://mastodon.se/@samuel).

Jag har lite grejer på [gitlab](https://gitlab.com/samuelskanberg) och på [github](https://github.com/samuelskanberg).