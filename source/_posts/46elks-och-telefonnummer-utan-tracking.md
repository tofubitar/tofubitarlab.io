---
title: 46elks och telefonnummer utan tracking
date: 2021-01-09 18:00:00
tags:
- voip
- sip
- sms
- virtuella nummer
- cell phone tracking
- övervakning
---
![](/images/elk.jpg)

Som jag skrev i [ett tidigare blogginlägg](/2021/01/02/hemsida-och-annat/) så försökte jag fixa ett virtuellt telefonnummer under julledigheten som man kan ringa och sms:a till och från. Anledningen till det är att slippa ha igång ett sim-kort som ständigt har kontakt med telefonmaster som därmed berättar för min telefonoperatör var jag är. Om jag kan ringa där jag har WiFi så är jag nöjd. (Och det är ju inget som hindrar att man har i ett kontaktkort med surf som man kan slå om man vill använda numret utanför WiFi).

Den här debatten om övervakning var stor i Sverige runt 2008 då [FRA-lagen](https://sv.wikipedia.org/wiki/FRA-lagen) röstades igenom i riksdagen. EFF har skrivit bra om problemen med [tracking via telefonmaster här](https://ssd.eff.org/en/module/problem-mobile-phones).

Vi är nog många som tagit till oss av den information som Snowden släppte och som inte är okej med den massiva övervakning vi utsätts för.

## Men vill ingen ha mina pengar?

Jag sökte runt länge och väl för att se om det fanns en möjlighet att köpa den här tjänsten. Men trots att IP-telefoni har funnits i många år och SIP standardiserades runt millenieskiftet så är det svårare än man kan tro att hitta någonstans på nätet som erbjuder:

* Ett telefonnummer som man kan sms:a till och från via sin smartphone och open source-appar
* Ett telefonnummer som man kan ringa till och från via sin smartphone och open source-appar

Några av de jag undersökte var:

* [Skype](https://www.skype.com/sv/features/call-phones-and-mobiles/)
* [Google voice](https://voice.google.com/)
* [Soluno](https://www.soluno.se/)
* [Quicknet](https://quicknet.se/)
* [voip.ms](https://voip.ms/)
* [Avoxi](https://www.avoxi.com/sms-virtual-phone-numbers/)
* [VirtualPhone.com](https://virtualphone.com/)
* [Grasshopper](https://grasshopper.com/)
* [Twilio](https://www.twilio.com/)
* [46elks](https://46elks.se/)

Skype och Google faller ju bort för att de inte är open source. Dessutom är de just sådana företag som jag inte vill ska ha kontroll och koll över min kommunikation. Genom [PRISM-programmet](https://en.wikipedia.org/wiki/PRISM_%28surveillance_program%29) så har NSA haft tillgång till data från amerikanska företag som [Google, Facebook och Apple](https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data). Så de stora jättarna känns inte så jättebra att förlita sig på.

Jag kontaktade några svenska IP-telefon-leverantörer (Soluno och Quicknet) och frågade om de hade någon lösning men de hade inget. De hade specifika SIP-lösningar och var huvudsakligen inriktade mot företag.

Några tjänster fungerade bara för folk i USA och Kanada (voip.ms, Avoxi och Grasshopper).

En tjänst (VirtualPhone) gav 500-fel när man skulle registrera sig.

Då återstod Twilio och 46elks. De är tjänster som har stöd för telefon och sms. Men fokuset är på deras API:er. De är alltså tjänster som man använder om man utvecklar programvara som ska ha stöd för telefon och/eller sms. Fokuset för dem är företag också.

Twilio verkade först lovande. Jag har använt deras tjänst tidigare för ett telefonhack till min port i flerbostadshuset där jag bor. Men tyvärr hade inte de nummer som hade stöd både för samtal och sms. Jag mailade dem men fick då reda på att svaret kunder dröja flera veckor eller månader.

## Kan 46elks fixa biffen?

Jag hade tidigare nosat lite på 46elks och hört om deras tjänster i mitt jobb. Så jag registrerade ett konto. De har ett trevligt API till sina tjänster och har en enkel och trevlig dashboard:

![](/images/46elks-dashboard.jpg)

Jag mailade dem och förklarade vad jag ville uppnå och jag fick ett väldigt positivt svar snabbt. Efter nyårshelgen ringde en person från 46elks för att prata mer om hur jag kunde uppnå det här. Han sa att det inte fanns en färdig tjänst som gjorde jag efterfrågade men att man kunde kombinera olika delar för att få det att fungera.

Lösningen skulle innefatta:

* Ett virtuellt mobiltelefonnummer som har stöd för sms och samtal (9 SEK/månad)
* Ett SIP-nummer (30 SEK/månad)

Sen konfigurerade de det virtuella numret så att när det kommer ett röstsamtal så kopplas det till SIP-numret. Det gjorde de genom callback-URL:erna: sms_url och voice_start. 

Sen får jag sätta upp en SIP-klient med de inställningarna jag fått för SIP-numret. Inställningar var:

* username
* password
* sip server
* uri

Då återstår bara att få det att börja lira, men det var lättare sagt än gjort.

### SMS

SMS är nog lite enklare tror jag. 46elks dokumentation om att [skicka och ta emot sms](https://46elks.se/products/two-way-sms) verkar vara rätt lätt att förstå. Så om man skulle behöva koda något själv så är det ingen större fara. Det som jag helst inte vill är dock att behöva drifta något själv på någon server. 

Men som tur är så var 46elks schyssta nog att låta mig använda deras beta-version av en webapp för att hantera SMS. Målgruppen är teams som kan dela ett nummer. Men det lär ju funka lika bra för mig. Det är ett rätt enkelt gränssnitt med inte jättemycket funktionalitet. Men det verkar funka:

![](/images/46elks-sms.jpg)

Det känns som att jag skulle kunna koda något enkelt själv om det skulle behövas.

Beroende på hur avancerat man vill ha sitt sms-program, om man ska köra med pushnotiser (vilket är ju typ så man får jobba med för att efterlikna sms) eller bara "kolla sms-inkorgen" genom att polla så kan man ju faktiskt klara allt med en webapp utan backend, som bara använder javascript.

Och om man bara använder SMS väldigt sporadiskt så räcker ju faktiskt [46elks logg](https://46elks.se/logs):

![](/images/46elks-sms-logs.jpg)

Och när man klickar på ett en rad så får man:

![](/images/46elks-sms-open.jpg)

Att verkligen få sms:en att bete sig som "vanliga" sms kan kanske vara svårare. Då behöver man nog gräva ner sig lite i sitt mobiloperativsystem. Det får bli en senare fråga om det skulle behövas.

Men SMS-delen känns ändå rätt bra.

### Telefonsamtal med SIP

Det här var något av en huvudvärk.

#### Linphone

Jag fick som sagt ett gäng inställningar. Jag laddade ner [Linphone](https://www.linphone.org/) för Android och testade på min telefon som kör [/e/os](https://e.foundation/) (en fri Android-version utan google-tjänster). Tyvärr så funkade det inte. "Anslutningen misslyckades".

Så jag testade med Linphone på min desktopmaskin som kör Ubuntu. Men det funkade inte heller. Jag använde deras wizard för att sätta upp nya konton.

Det var svårt att få till att ringa över huvud taget. Telefonnummer gick inte alls. Programmet reagerade inte alls. Om jag skrev in ett nummer som började med +46 eller phone:+46 så gick det inte alls. Men SIP-nummer accepterade i alla fall GUI:t.

Men då kom problemet att hitta ett test-nummer som är aktivt och som man kan ringa. Till slut [hittade jag](https://www.voip-info.org/phone-numbers/) följande SIP-nummer som jag kunde ringa:

* [sip:thetestcall@iptel.org](sip:thetestcall@iptel.org)

Jag hade nytta också av WebRTC-klienten [FreePhoneBox.net](https://freephonebox.net/). Med den så kunde jag testa vilka SIP-nummer som funkade och som alltså också borde funka med Linphone om jag konfigurerat allt korrekt.

Vissa problem uppstod med WebRTC-klienten. T.ex. fick jag "Incompatible SDP" som felmeddelande på vissa SIP-nummer. Det finns flera saker att ta höjd för när man pysslar med SIP.

Men numret [sip:thetestcall@iptel.org](sip:thetestcall@iptel.org) funkade inte för mig i Linphone. Så med andra ord fick jag ingenting att funka med Linphone.

#### Sipdroid

Det här verkade lovande för den tog över själva vanliga samtalsförfarandet. Alltså, om jag öppnar kontakter och ska ringa min andra telefon, då dykte inte den vanliga ringvyn upp utan då öppnades Sipdroid... egentligen inte riktigt vad jag ville eftersom jag ju inte satt upp det korrekt :) Nåja, jag kunde ändra tillbaka det genom att avinstallera appen.

#### Androids inbyggda stöd för SIP - WiFI calling

Jag hade hört från en kompis att stöd för SIP [finns inbyggt i Android](https://support.google.com/phoneapp/answer/2811843?hl=en&visit_id=637455490795628979-1253640430&rd=1). (Och eftersom /e/os är en Android-fork så funkar det för mig).

Och det var verkligen smidigt att ställa in! Så när jag väl får SIP att funka på telefonen kommer jag nog att använda den inbyggda klienten istället för Linphone.

#### Är det ens rätt inställningar?

Själva kopplingen mellan det virtuella numret och SIP-numret funkade inte så bra, eller så var det min uppsättning av SIP-kontot på min telefon. 

Jag fick också veta att 46elks SIP-nummer inte har stöd för närvarande för utgående samtal. Man måste hacka runt lite för att få det att funka på ett bra sätt. Så det kan ha varit så att en del av felen som uppstod som jag beskrivit ovan kanske inte hade hänt om jag hade använt en annan SIP-trunk.

## Men jag är nog ändå på rätt väg

Jag hade som sagt mailat ett gäng företag som jag tänkte borde veta vad jag menar. Nu fick jag svar från [Teltek](http://teletek.se/):

> Du beskrev ditt ärende bra tycker jag så jag förstår vad du tänker dej. Egenligen kan vi leverera precis en sådan här tjänst men det är bara företag som är kunder hos oss på en sådan här sak. Vi sätter då upp en SIP-trunk åt kunden med önskat antal kanaler (=samtidiga samtal). Vi knyter ett sveskt mobilnummer till tjänsten (istället för ett fast telefonnummer som det brukar vara när det gäller IP-telefoni) och dessutom så sätter vi upp en SMS-tjänst med mobilnumret. Man kommer åt SMS-tjänsten genom ett webblogin eller via API till det egna affärssystemet. SIP-trunken lägger man sedan in i företagets telefonväxelsystem, ofta en Linux-baserad Asterisk-växel.
​ 
> Det här har en startkostnad på 8 000 kr för uppsättningen av SMS-systemet och kopplingen av mobilnumret till det, månadskostnaden för SMS-tjänsten är sedan 199 kr/mån. SIP-trunken med mobilnumret har ingen engångskostnad men en månadskostnad på 240 kr/mån. Kostnader för utgående samtal och SMS tillkommer, inkommande samtal och SMS är gratis.

Teltek kan alltså erbjuda vad jag vill ha! Men det var alltså för en rätt saftig peng, alltför saftig för ett privat telefonnummer. Men svaret gav mig ändå lite glädje i att det visar att jag nog ändå är på rätt väg. Jag fortsätter kontakten med 46elks och ser om de kan hitta andra sätt.

Vet du hur man sätter upp SIP för 46elks? Eller har du tips på en annan lösning? Skicka då gärna ett meddelande till mig på [@samuel@mastodon.se](https://mastodon.se/@samuel).

Fortsättning följer...