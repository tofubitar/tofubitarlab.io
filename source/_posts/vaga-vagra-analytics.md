---
title: Våga vägra analytics
date: 2021-01-08 18:28
tags:
- web analytics
- google analytics
- övervakning
- integritet
- goatcounter
- matomo
- plausible
- umami
---
![](/images/google-eyes.jpg)

Hur många läser bloggen? Det är ju klart man undrar. Och om man är lite sucker för statistik så vill man ju ha statistik över besökarna :)

Lite tanken med den här bloggen var för mig att gräva mig ner mer i övervakningsfrågor och intergritetsvänliga tekniker. Så att slänga på ett google analytics-script skulle ju inte kännas så bra.

[Google trackar en](https://www.wired.com/story/google-tracks-you-privacy/) sjukt mycket. EFF har skrivit mycket om hur mycket man blir trackad på nätet. De har till och med gjort ett trevligt plugin som skyddar dig från tracking: [Privacy badger](https://www.eff.org/deeplinks/2019/07/sharpening-our-claws-teaching-privacy-badger-fight-more-third-party-trackers). Men man ska inte behöva ha ett plugin installerat i sin webbläsare för att slippa integritetskränkande tracking på min hemsida.

Jag har kollat runt på olika alternativ och det finns [massor av bra alternativ](https://alternativeto.net/software/google-analytics/)! För mig så är det självklart att det ska vara fri mjukvara.

Några alternativ som jag blivit tipsade om är:

* [Plausible](https://plausible.io/)
* [Matomo](https://matomo.org/)
* [Umami](https://umami.is/)
* [GoatCounter](https://www.goatcounter.com/)

Plausible verkar vara najs. Enkelt, snyggt och har allt jag skulle vilja veta. Man kan antingen registrera sig på deras hemsida (billigaste 6$/månad) eller hosta själv. 

Samma sak med Matomo. Det ser enkelt och snyggt ut. Billigaste alternativet hos dem är 319 SEK/månad.

Umami har ingen betalversion. Där får man hosta själv. Men de tipsar om [free tier](https://umami.is/docs/hosting) hos t.ex. Netlify. Så det kunde man kika på.

Självklart ska fri mjukvara få kosta pengar men jag använder den här bloggen mest som dagbok. Så jag vill inte betala 6$/månad eller betala för att hosta själv bara för att få statistik till den här bloggen.

Och då blev jag tipsad om GoatCounter. Det är inte det vackraste gränssnittet men det visar tillräckligt med information för mig.

![](/images/goatcounter-screenshot.png)

Så nu kör vi på med GoatCounter ett tag. Om bloggen (mot förmodan) skulle växa enormt och jag skulle vilja ha lite mer data så kanske jag kikar jag några av de andra lösningarna.