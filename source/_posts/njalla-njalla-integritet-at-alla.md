---
title: Njalla, njalla! Integritet åt alla
date: 2021-01-28 21:00:00
tags:
- domäner
- domains
- loopia
- njalla
---
![](/images/njalla.jpeg)

(Bilden är CC-SA-3.0 från [Wikipedia](https://commons.wikimedia.org/wiki/File:Gillesnuole_njalla2.jpg))

Nu är det inte längre Loopia som gäller. Nu är det [Njalla](https://njal.la) som gäller! 

Jag berättade i förrra blogginlägget om att de 129 kr / år hos Loopia för en domän i slutändan blev 281 kr / år. Om jag hade vetat det på förhand så hade jag direkt reggat hos Njalla. En domän på njalla kostar 30 euro per år alltså runt 300 kr / år.

Nåja, gjort är gjort. Men det positiva är att hos Njalla såg jag att domänen bitar.se var ledig så den reggade jag snabbare än snabbt. Och det var ju faktiskt det jag ville ha från början. För med en sådan domän kan jag ha fixa mailadressen samuel@bitar.se (ännu inte gjort dock) och det är ju rätt häftigt ändå :)

Så numera finns den här bloggen på [https://tofu.bitar.se](https://tofu.bitar.se).

Njalla erbjuder för övrigt flera tjänster:

* Domäner (från 15 euro per år)
* VPS (från 15 euro per månad)
* VPN (5 euro per månad)

Så kika in Njalla!

## Youtube-dl

En helt annan grej som jag vill dela med mig av - youtube-dl!

Av någon anledning så tog det lång tid för mig att komma igång med [youtube-dl](https://youtube-dl.org/). Youtube-dl används för att ladda ner videos (eller bara ljud) från youtube lokalt till din dator.

Men nu är det check! Jag försöker ju de-googla mitt liv så jag vill inte gärna sitta på youtube hela dagen bara för att lyssna på musik. Och det känns ju lite dumt att strömma samma musik om och om igen när jag faktiskt kan spara ner den.

Jag är ju en sucker för bitpop så jag drog ner Monkey island av [Dubmood](http://www.dataairlines.net/dubmood/):

```bash
youtube-dl --extract-audio --audio-format mp3 "https://www.youtube.com/watch?v=11yewfkUSQM"
```

Finns härlig Dubmood [merch att köpa här](https://dataairlines.bandcamp.com/merch/).

En annan härlig grupp är ju [047](https://sv.wikipedia.org/wiki/047). Så här kan man dra ner albumet Elva från youtube:

```bash
youtube-dl --extract-audio --audio-format mp3 --yes-playlist "https://www.youtube.com/watch?v=poUTPLVmCbo&list=OLAK5uy_nJhU00s5ZTJCImbDC_rdKOb4Ggq7KCW0s"
```
