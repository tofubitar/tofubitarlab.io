---
title: Web/rss/atom-flöde och tankar om hemsidan
date: 2021-01-06 18:50:00
tags:
- rss
- atom
- feed
---
![](/images/feed-icon.png)

Så nu har jag fixat sånt där webbflöde. Eller kallar man det RSS-flöde? Det är ju Atom, men jag har nog aldrig hört någon säga atom-flöde.

Man kan gå direkt till [flödet här](/atom.xml) eller så kan man klicka på den fina lilla ikonen längst ner på sidan. Det visade sig att det var inbyggt i det [temat](https://github.com/zchengsite/hexo-theme-oranges) jag kör.

Själv har jag inte prenumererat så mycket på webbflöden men två personer efterfrågade det så det kanske är något man ska kika på ändå.

## Tankar om hemsidan

Jag är pepp på den här hemsidan får jag ändå säga. Trots att jag har hållit på med datorer, teknik, programmering och linux sedan länge så har jag inte haft en personlig blogg tidigare. Jag fixade en lite [roligare hemsida](https://samuelskanberg.github.io/) till min github men det är ju inget jag har använt för att blogga. Och så har jag skrivit många blogginlägg för min partiförening och andra olika nätverk. Men alltså ingen personlig. Men nu så!

Jag har också landat i att jag skriver det här mest för min egen del. Det blir som en lite nördigare dagbok. Jag blev inspirerad av en person som skrev om hållbar aktivism om att föra en tacksamhetsdagbok varje dag där man skriver tre saker som man är tacksam eller glad över. Jag har nog inte varit så mycket för dagböcker tidigare men det har varit najs att göra.

Sen är det ju skoj att dela med sig också av ens tankar, saker man läst och projekt man pysslat med. Och om man tur så kan någon annan få glädje av det.

Några saker som jag tänker pyssla med:

* Rensa bort temat från onödiga grejer som javascript som inte behövs
* Fixa statistik för hemsidan. Antagligen blir det [Plausible](https://plausible.io/) eller [Umami](https://umami.is/)
* Blogga vidare om jakten på det övervakningsfria telefonnumret
* Blogga om några roliga programmeringsprojekt jag gjort
* Ha ett integritetsår (i stil med ett "köpfritt år" eller "flygfritt år") och blogga om det

Om du har några tips på saker att ta upp så [skriv gärna några rader](https://mastodon.se/@samuel).