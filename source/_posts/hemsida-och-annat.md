---
title: Hemsida och annat
date: 2021-01-02 16:06:14
tags:
- programmering
- javascript
- övervakning
- gitlab
- voip
---
Nu under julledigheten så har jag haft ordentligt tid med att fixa med lite ditt och datt. Några grejer som är på gång eller snart är på gång:

* Hemsida - check
* Göra en MVC-app med vanilla javascript - [check](https://github.com/samuelskanberg/Javascript-MVC)
* Fixa ett [gitlab-konto](https://gitlab.com/samuelskanberg) för hobbyprojekt så man slipper vara alltför beroende av github. De kör ju trots allt en massa proprietär mjukvara.
* Fixa VOIP med telefonnummer som klarar både sms och samtal till och från

## Hemsida

Ja, här är den: Tofubitar. Eftersom jag är vegan så gillar jag tofu. Och tofu serveras ofta i kuber, eller helt enkelt "bitar". Och data har vi ju i bitar också. Kanske inte helt orginellt ändå, om man tänker på att filmen [Livet i 8 bitar](https://www.imdb.com/title/tt0323084/) redan funnits ett tag... men ändå.

Jag tänkte jag skulle kolla på det här med static site generators. Jag var mest inne på att köra [Jekyll](https://jekyllrb.com) som github pages länge har haft stöd för. Men den kör i Ruby så där var jag redan lite skeptisk. Jag använder inte Ruby till något annat så känns onödigt. Men det löste sig automatiskt! Hur då? Jo, genom att jag inte lyckades installera det :| Med tanke på att det finns numera en [uppsjö](https://itsfoss.com/open-source-static-site-generators/) av static site generators så gick jag vidare i listan. Jag gillar python så det skulle vara najs med en som kör det. Men [den listan](https://www.fullstackpython.com/static-site-generator.html) som jag hittade med static site generators i python var tyvärr inte en av de mest populära. Så jag tog istället [hexo](https://hexo.io/) som är skriven i javascript.

Jag hittade ett snajdigt tema som jag anpassade med en egen logga innehållande tofu :) Finns en [kort beskrivning här](https://gitlab.com/tofubitar/tofubitar.gitlab.io) om hur jag gjorde.

## MVC-app med vanilla javascript

En nackdel med att vara webbutvecklare är att man översvämmas av en himla massa nya tekniker och bibliotek. Det kan vara svårt att veta hur många av de bibilioteken kommer att vara kvar om 5 eller 10 år. Jag är ju något lat av mig så jag vill helst lära mig saker som jag kan ha nytta av i alla fall i 15 år.

Hur som helst, därför tänkte jag det vore najs att lära mig att göra webbapp mha MVC-design i bara javascript. Jag tog hjälp av [den här](https://medium.com/swlh/writing-a-simple-mvc-app-in-vanilla-javascript-a30a68a15a1) sidan. 

Jag kopierade friskt och gjorde mig en egen webbapp som ska vara en [shoppinglista](https://github.com/samuelskanberg/Javascript-MVC). Pga CORS-problem som uppstår när man kör module i javascript så kunde jag inte bara köra webbappen genom file:// så jag fick fixa upp det med docker. Känns ju lite löjligt med tanke på att allt är frontend. Sen pallade jag inte riktigt göra en hemsida som kör koden. Men men, det kan ju komma senare.

## Gitlab-konto

Trots att det finns sjukt många open source projekt på github så är ju inte plattformen github open source. Gitlab är ju open source så därför ville jag hellre köra det.

Vi kör gitlab på jobbet och det fungerar utmärkt! Men jag har ett gäng projekt på [github](https://github.com/samuelskanberg) så jag kanske inte vill gå över helt än.

Det finns en del bra jämförelser mellan gitlab och github, se t.ex. [den här sidan](https://hackernoon.com/github-vs-gitlab-which-is-better-for-open-source-projects-31c45d464be0). Jag blev faktiskt förvånad över att gitlab verkar riktigt bra jämfört med github. Sen vet jag att mycket har hänt sedan dessa bloggposter skrivits. Men jag trodde att jag fick välja mellan "fri mjukvara men halvkasst" och "bra funktionalitet men proprietärt". 

Gitlab har själva [gjort en jämförelse](https://about.gitlab.com/devops-tools/github-vs-gitlab/decision-kit/). Självklart är den vinklad men det är hur som helst stöd för mycket.

Otroligt mycket ingår också i gratis-versionen av gitlab. 

## VOIP-nummer

Det här har varit den stora utmaningen! 

Som vi alla har vetat ett tag nu så spåras vi automatiskt genom telefonerna vi går runt med. EFF har många artiklar på ämnet [cell tracking](https://www.eff.org/issues/cell-tracking). De har skrivit en riktigt bra bloggpost med titeln [The Problem with Mobile Phones](https://ssd.eff.org/en/module/problem-mobile-phones).

Med andra ord, jag vill inte gärna gå runt med en apparat som spårar mig om jag inte behöver det. Men samtidigt vill jag att man ska kunna ringa mig och skicka sms till mig. Min lösning är som följer, skaffa ett nytt nummer:

* ...som går via internet istället för telefonmastarna
* ...som börjar med 07 (så att folk och dumma system förstår att det är ett mobilnummer)
* ...som man kan ringa till och från
* ...som man kan sms:a till och från
* ...som jag kan använda från min mobiltelefon
* ...som jag kan använda med fri mjukvara
* ...som inte behöver telefonmaster eller SIM-kort alls

På så sätt kan jag ha ett telefonnummer när jag har tillgång till WiFi. Jag skulle se till att ha ett kontantkort med surf på som jag har möjlighet att slå på om jag verkligen skulle behöva använda mitt digitala nummer. Men då öppnar jag ju upp för övervakning under den tiden, men det görs ju som undantag. 

Jag har sökt runt som en galning men hittar ingen färdig lösning! Det närmaste är väl Skype out. Men det är ju inte direkt fri mjukvara. Några andra uppslag är:

* Twilio
* 46elks
* Andra VOIP-lösningar som [Soluno](https://www.soluno.se/) eller [Quicknet](https://quicknet.se/)

Jag kom väldigt nära med Twilio. Men det föll på det faktum att jag inte kunde köpa ett nummer hos dem som har stöd både för samtal och sms. Men jag har använt Twilio förr för att lösa telefonsaker så jag mailade dem för att fråga. (Men jag kan vänta mig flera veckor eller månader innan de svara säger de).

Då mailade jag [46elks](https://46elks.se/) som har liknande tjänster som Twilio har. 46elks är ett svenskt företag och jag träffade någon gång en trevlig utvecklare därifrån flera år sedan. Jag mailade dem och fick ett mycket trevligt svar där de lovar att kolla om de kan hitta någon lösning som passar.

Jag försökte också att ta hjälp av kamraterna på mastodon. Och så kontaktade jag också [Trevlig mjukvara](https://trevligmjukvara.se/) för att höra om de kunde hjälpa till.

Än så länge så verkar 46elks vara det bästa alternativet. 

Har du tips på någon bra lösning? Skicka då gärna ett meddelande till mig på [@samuel@mastodon.se](https://mastodon.se/@samuel).

Happy hacking!