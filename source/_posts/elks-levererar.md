---
title: Älgarna levererar... virtuella telefonnummer
date: 2021-01-16 18:00:00
tags:
- voip
- sip
- sms
- virtuella nummer
- cell phone tracking
- övervakning
---
![](/images/moose-talking.jpg)

Nu har jag fått det att funka med virtuella telefonnummer med 46elks! Jag har skrivit om det i ett [tidigare blogginlägg](/2021/01/09/46elks-och-telefonnummer-utan-tracking/) då jag ännu inte fått det att fungera. Men nu kan jag fira att ett trevligt projekt tagits i hamn.

## Vad är det som funkar?

Jag ville hitta ett sätt att kunna ha ett telefonnummer som man kan sms:a och ringa till och från. MEN jag ville att det skulle vara virtuellt så att trafiken inte behöver gå över telefonmasterna. Varför? Jo, för att undvika att min telefon blir en [övervakningsapparat](https://ssd.eff.org/en/module/problem-mobile-phones).

I mitt förra blogginlägg så skrev jag om hur jag letade runt efter olika tjänster för det här. Till slut beslutade jag mig för att köra vidare på [46elks](https://46elks.se). Företaget erbjuder ett [API](https://46elks.se/docs/overview) med olika [tjänster](https://46elks.se/pricing) kopplat till telefonsystemet som att skicka/ta emot sms och koppla samtal.

## SMS

SMS var det enklaste att få att fungera. Jag fick tillgång till deras webbapp som man kan logga in på för att läsa och skicka sms. Ett rätt enkelt program men som gör vad det ska:

![](/images/46elks-sms.jpg)

Men egentligen så behöver man inte ha tillgång till ens det. Man kan skicka sms direkt från dashboarden:

![](/images/46elks-dashboard2.png)

Om man bara använder SMS väldigt sporadiskt så räcker ju faktiskt [46elks logg](https://46elks.se/logs):

![](/images/46elks-sms-logs.jpg)

Och när man klickar på ett en rad så får man:

![](/images/46elks-sms-open.jpg)

Men man får inget "pling" när man får ett sms. Man måste surfa in själv och kolla. Min plan för sms är nog:

* Gör en enkel webapp i html och javascript som kan ersätta 46elks egen webapp och som jag kan ha direkt på min telefon
* Utöka webappen med pushnotiser eller gör något motsvarande i Android

## Telefon

46elks erbjuder en SIP trunk som kan knytas till ens [virtuella telefonnummer](https://46elks.se/products/virtual-numbers). Man får då ett SIP-konto, vars inloggningsuppgifter man matar in i sin SIP-klient.

Jag har fått inkommande och utgående samtal att fungera med SIP-klienten Linphone. Det går alldeles utmärkt att ringa till det virtuella numret som kopplar vidare till SIP:

![](/images/linphone-calling.jpg)

### Utgående samtal kräver lite hackande

Tyvärr har 46elks inte stöd att skapa utgående samtal från ens SIP-klient. Det som man måste göra är att köra ett script som kopplar upp samtalet. Det som händer då är att ens SIP-klient ringer och när man svarar, DÅ rings det numret upp som man ville ringa. Lite omständigt minst sagt och gör det definitivt mycket svårare att använda.

Här är ett script i python för att koppla upp ett sådant samtal:

```python
#!/usr/bin/env python3
import requests
import json
import os
import sys

if len(sys.argv) != 2:
    print("Usage: {} phone_number".format(sys.argv[0]))
    sys.exit(1)

# Get username and API from https://46elks.se/account"
API_USERNAME = os.getenv("ELKS_API_USERNAME")
API_PASSWORD = os.getenv("ELKS_API_PASSWORD")
VIRTUAL_NUMBER = os.getenv("ELKS_VIRTUAL_NUMBER")
SIP_NUMBER = os.getenv("ELKS_SIP_NUMBER")

auth = (API_USERNAME, API_PASSWORD)

# The phone number to call, should be a string like: '+46738123456'
json_connect = {'connect': sys.argv[1]}

fields = {
    # virtual number, should be a string like: '+46766123456'
    'from': VIRTUAL_NUMBER,
    # sip number, should be a string like: '+4600111234'
    'to': SIP_NUMBER,
    'voice_start': json.dumps(json_connect)
    }

response = requests.post(
    "https://api.46elks.com/a1/calls",
    data=fields,
    auth=auth
    )

print(response.content)
```

Man anropar scriptet på följande sätt:

```bash
ELKS_API_USERNAME="abc123" ELKS_API_PASSWORD="abc213" ELKS_VIRTUAL_NUMBER='+46766123456' ELKS_SIP_NUMBER='+4600111234' ./outgoing_sip_.py '+46738123456'
```

Då ringer det i ens SIP-klient. Och när jag svarar så rings det telefonnummer upp som jag vill ringa. Det funkar inte helt felfritt dock. Någon gång ringde det inte och jag fick failed i loggen hos 46elks. Någon annan gång tog ~10 sekunder för att Linphone skulle börja ringa.

Och det självklara är att det här vill man ju köra på sin telefon. Så antingen att man kör det här python-scriptet i en python-miljö på telefonen eller gör motsvarande med någon android-app. Gärna en app som har stöd för att kunna kolla i telefonboken. 

Min plan är nog att göra en webapp som gör samma sak som python-scriptet.

## UDP, TCP, TLS

Som jag skrev i förra blogginlägget så fick jag det inte att funka tidigare med Linphone eller någon annan SIP-klient. Jag tror att det kan ha varit för att jag kanske valde TLS istället för UDP i inställningarna för Linphone. För när jag testar att slå på TLS nu så slutar det att funka men när jag kör med UDP så funkar det bra.

Det är ju också en fråga om säkerhet så klart. Om alls går okrypterat så är det ju mindre gött. Det får jag kolla med 46elks. 

## Inställningar på 46elks

Om man vill replikera det här så behöver man alltså skaffa:

* Ett virtuellt mobilnummer med stöd för SMS och samtal
* Ett SIP-nummer

Det finns två callback url:er för det virtuella numret: sms_url och voice_start. sms_url satte jag till url:en för den tjänst som jag fick låna av 46elks. Och voice_start ska sättas till {"connect":"+4600111234"} (där numret är numret till ditt SIP-nummer).

Call back url:en för SIP-numret voice_start använde jag inte ens eftersom jag använde SIP-klient.

## Priser

* Virtuell mobilnummer - 9 SEK/månad
* SIP-nummer - 30 SEK/månad

Ta emot samtal och SMS är gratis. Att skicka SMS kostar 35 öre. Samtal i Sverige till fasta nätet är 15 öre per minut till fasta nätet och 57 öre per minut till mobilnätet. [Här är hela prislistan](https://46elks.se/pricing).

## Avslutning

Så jag fick det att funka till slut! Det känns riktigt bra :) Några saker behövs för att jag faktiskt skulle börja använda det här numret:

* Pushnotifikationer för SMS
* Kunna ringa från SIP-klienten (eller i alla fall hitta en väldigt smidig lösning för att koppla samtal)
* Vara säker på att telefonsamtalen krypteras

Återigen, man behöver internet för att kunna nyttja det här numret. Så om man vill ringa och skicka/ta emot sms när man är utanför WiFi så behöver man ett sim-kort med surf som man kan aktivera när man behöver internet. Jag tänker att man kan skaffa ett kontantkort. Men även om man kan köpa kontantkort anonymt så exponerar man sig och antagligen finns det tillräckligt mycket man gör på nätet med sin mobil som gör att det kan knytas till en. En variant är ju att man låter allt gå via en VPN så att allt blir krypterat. 

Den stora behållningen jag fick av det här äventyret var nog att det är enkelt att sätta upp telefonnummer för SMS och att det finns bra tjänster (som 46elks och Twilio) som kan göra livet smidigt för en. Om man bara klarar sig med SMS (för registreringar till sina konton, 2FA, osv) så funkar det utmärkt.

Om du testar det här också får du gärna berätta om hur det gick! Skicka ett meddelande till mig på [@samuel@mastodon.se](https://mastodon.se/@samuel).

Happy hacking!