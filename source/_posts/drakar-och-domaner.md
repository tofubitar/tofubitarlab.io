---
title: Drakar och domäner
date: 2021-01-17 18:00:00
tags:
- domäner
- domains
- loopia
- njalla
- protonmail
---
![](/images/domain-names.jpg)

Sådär! Nu har jag fixat en domän till bloggen. Snart kan man läsa bloggen på [www.tofubitar.se](https://www.tofubitar.se) istället för [tofubitar.gitlab.io](https://tofubitar.gitlab.io). (Men på grund av lite strul med gitlab så kan jag inte skapa upp domänen på gitlab).

Det var en kollega som tyckte att jag borde regga en domän så att jag inte behöver vara knuten till gitlab om jag vill byta i framtiden. Det är ju en rätt sund tanke. Samtidigt så är min kollega en hejare på att regga domäner. Som han själv sa "man sitter där i lokaltrafiken hem på kvällen och har en teknikdidé och reggar där och då en domän för projektet som man tänker man startar i morgon". Han var rätt medveten om att det blev fler domäner reggade än projekt som blev avslutade :)

## Egen domän för epost-adress

Jag var i valet och kvalet över om jag skulle försöka hitta en domän som både kunde duga som namn till den här bloggen och som kunde funka till mail. Jag kör Protonmail men har inte helt bytt för jag vet inte om jag verkligen ska köra på Protonmail.

Är du själv sugen på ProtonMail? Här är några artiklar om varför det kan vara bra eller mindre bra.

* [Protonmail om sig själva](https://protonmail.com/why-protonmail)
* [ProtonMail review: have we found the most secure email provider in 2021?](https://cybernews.com/secure-email-providers/protonmail-review/)
* [PCMag](https://uk.pcmag.com/privacy/122654/protonmail)
* [Outrun the Bear: ProtonMail is Not for Activists](https://cldc.org/protonmail/)
* [ProtonMail Review: Free Secure Email Service](https://www.lifewire.com/protonmail-review-4107075)

Hur som helst, nu kör jag på Protonmail ett tag. Och liksom många andra epostleverantörer så kan man uppgradera och använda en egen domän. Så jag KAN ju använda samuel@tofubitar.se men jag tänker ändå att det är en epost-adress som jag nog inte kommer att vilja använda i alla mina sammanhang.

## Loopia

Det blev i alla fall en domän hos Loopia. Det verkar vara det som många kör och är nöjda med. Och jag är tillräckligt konformist för att göra på det sätt som alla andra gör.

Så nu har jag betalat för tofubitar.se. MEN jag är ändå lite irriterad. Jag kollade på deras sida och då kostar en .se-domän 129 kr/år. Det är ju rätt najs tänker jag... sen tillkom ju moms. Okej, ett klassiskt misstag men ändå. Det är okej. Men sen så visar det sig att jag inte kan ändra domänen till vad jag vill om jag inte uppgraderar till LoopiaDNS som kostar 120 kr/år (inkl moms)!

😠

.se-domänen kostade mig alltså 281 kr/år.

## Njalla

När jag frågade på Mastodon om vad för tjänst jag skulle använda för domäner så föreslog många Loopia. Men en person uppmärksammade mig om [Njalla](https://njal.la/).

Det är en tjänst som startats av Peter Sunde, en av grundarna bakom The Pirate Bay. Grundarna till Njalla [skriver själva](https://njal.la/blog/opening/) om varför de öppnat tjänsten. Här är ett utdrag:

> In the world where peoples right to privacy and the right to be anonymous is under attack, we’re helping you to fight back. njal.la is a service that helps you to have a domain name without having to tell the world who you are. We register domain names in hundreds of top level domains totally anonymous. Yeah you read right. We don’t need to know who you are, what you are, where you are. We don’t even need an e-mail address — we can do with an anonymous XMPP account you can set up somewhere.
> [...]
> As long as you keep within the boundaries of reasonable law and you're not a right-wing extremist, we’re for promoting your freedom of speech, your political weird thinking, your kinky forums and whatever. Even Trump is welcome. Hell, he might even be a customer. We’ll never know. We might even be approved by him! Or not. We don’t really care.

 Och här finns lite fler länkar:

* [Peter Sunde om sin nya tjänst – "för alla som bryr sig om sitt privatliv"](https://www.idg.se/2.1085/1.680843/njalla-peter-sunde-anonymt-domaner)
* [Peter Sundes nya tjänst hjälper aktivister bekämpa spansk censur](https://www.nyteknik.se/digitalisering/peter-sundes-nya-tjanst-hjalper-aktivister-bekampa-spansk-censur-6890280)
* [Peter Sunde drar igång domänföretaget Njalla För kunder som vill registrera domäner anonymt](https://feber.se/webb/peter-sunde-drar-igang-domanforetaget-njalla/364306/)

De tar betalt i Bitcoin, Litecoin, Monero, ZCash, DASH, Bitcoin Cash, Ethereum och Paypal. 

.se-domäner kostade hos dem 30 euro/år. Det var innan jag förstått att Loopia kostade 281 kr/år. 

Men jag tänker att nästa domän jag reggar ska jag försöka regga hos Njalla. Förhoppningsvis tar de inte extra betalt för NjallaDNS eller någon annan fint som Loopia gjorde.

## TPB AFK

Det blir ju lätt att man börjar tänka på The Pirate Bay när man tänker på Peter Sunde. Om ni inte redan har sett dokumentären TPB AFK så är den ju trevlig :) Man kan se på [peertube](https://peer.tube/videos/watch/f30157a5-14bd-4fe4-b565-021ccd676ee2), [libreflix](https://libreflix.org/assistir/tpb-afk) eller på [youtube](https://www.youtube.com/watch?v=eTOKXCEwo_8).

![](/images/tpb-afk.jpg)