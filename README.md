# tofubitar.gitlab.io

This is the web site for Tofubitar using the static site generator hexo.

It is deployed at [gitlab pages](tofubitar.gitlab.io). Later also on [tofubitar.se](tofubitar.se).

The setup from scratch was:

```
npm install hexo --save
# Setup the PATH so we can use hexo directly
nano ~/.bashrc
# Add to the end: PATH="$PATH:./node_modules/.bin"
source ~/.bashrc
hexo init blog
rm -rf node_modules/ package-lock.json
mv blog/* .
rm -rf blog
hexo server
```

Then we did gitignore and other stuff.

Set up theme:

```
git clone https://github.com/zchengsite/hexo-theme-oranges.git themes/oranges
# edit _config.yml and set "theme: oranges"
rm -rf themes/oranges/.git*
# Download and fix image to logo.png: https://pixabay.com/vectors/asian-cubes-culture-dish-eat-food-1294263/
cp path/to/your/logo/file.png themes/oranges/source/images/avatar.png
```